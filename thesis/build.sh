#!/bin/bash

if [[ -z "$1" ]]
then
	echo -e "\e[31mError: no target specified! Aborting\e[0m";
	exit 1;
fi

baseName=$(echo "$1" | cut -d "." -f 1);
echo -e "\e[32mBuilding pdf for $baseName\e[0m";

logs=$(pdflatex -halt-on-error --shell-escape $baseName && bibtex $baseName && pdflatex -halt-on-error --shell-escape $baseName && pdflatex -halt-on-error --shell-escape $baseName)

if [[ "$?" = "0" ]]  
then
	echo -e "\e[32mBuild successfull\e[0m";
else
	printf "%s\n" "$logs";
	echo -e "\e[31mError during build\e[0m";
	exit 1;
fi

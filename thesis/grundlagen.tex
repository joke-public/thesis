%% grundlagen.tex
%% $Id: grundlagen.tex 61 2012-05-03 13:58:03Z bless $
%%

\chapter{Basics}
%% ==============================
%Die Grundlagen müssen soweit beschrieben
%werden, dass ein Leser das Problem und
%die Problemlösung  versteht.Um nicht zuviel 
%zu beschreiben, kann man das auch erst gegen 
%Ende der Arbeit schreiben.

\section{Software Defined Networking (SDN)}
Software Defined Networking refers to networks, where the control and data plane are separated.
The most common architecture for SDN is OpenFlow.
In OpenFlow, every switch contains one or more so called flow tables and an abstraction layer that communicates with the controller.
The controller deploys the flow table entries and the switches forward traffic as configured in their flow tables.
For more information about SDN, see~\cite{sdn_2014}.


\section{IEEE 802.3 Ethernet}
% Ethernet is defined in IEEE 802.3
% IEEE 802.3y: 100 Mbit/s (100BASE-T2)
% IEEE 802.3ab: 1000 Gbit/s (1000BASE-T)
Ethernet is the currently dominant network standard in both office environments and data centers.
It is fast, easy to install and the equipment is cheap.
\\
When a package is received by an Ethernet switch, it is put in the package's target egress port's queue.
The target port is determined by matching the MAC address of the package and possibly other headers against the forwarding table of the switch.
If this queue is full, the package gets dropped.
Although there can be multiple prioritized queues per egress port in standard Ethernet, the packages of the same priority class can still be delayed by previous packages in their queue.
Because there are no mechanisms in standard Ethernet for reserving bandwidth for real-time flows, it can't give any guarantees for jitter, end-to-end delay and package loss.
This is a problem for critical systems, that depend on each package arriving in time.~\cite{indEther_2005}



\section{Real-Time in Networking}
Many network applications, for example machine control, demand an ultra low latency, package loss and jitter.
If the low jitter, latency and package loss is only statistically guaranteed, this constraint is called soft real-time, whereas if all packages must meet the jitter and latency requirement and no package loss is allowed, it is called hard real-time.
\\
Networks capable of achieving real-time constraints can be build by reserving bandwidth for each real-time flow.
Another possibility is to use a designated link for each flow, so they can't interfere with each other, but this approach scales poorly.
Therefore this thesis focuses on the shared network with bandwidth reservation approach.
This approach requires specialized hardware, that can prioritize traffic and reserve bandwidth.~\cite{ull_2019}



\section{IEEE 802.1 Time-Sensitive Networking (TSN)}
TSN refers to a set of standards specified by the IEEE 802.1 Time-Sensitive Networking Task Group.
Before 2012 the group had the name \emph{Audio/Video Bridging} (AVB) Task Group and worked on standards focusing on audio and video data transmission.
Then they added more general standards, that were needed to get the IEEE 802.1 set ready for industrial usage, which required for example fault tolerance, which is not present in the AVB standards~\cite{ft_tsn_2014}.
Detailed information and a performance analysis of AVB can be found in~\cite{avb_perf_2012}.

This theses uses the following standards in particular:
\begin{itemize}
  \item IEEE 802.1AS Timing and Synchronization for Time-Sensitive Applications (gPTP)
  \item IEEE 802.1Qbu Frame Preemtion
  \item IEEE 802.1Qav Forwarding and Queuing for Time-Sensitive Streams (FQTSS)
  \item IEEE 802.1Qbv Enhancements for Scheduled Traffic
\end{itemize}

IEEE 802.1AS defines how Ethernet, Wi-Fi and MoCA (Multimedia over Coax Alliance) can all be part of the same IEEE 1588-2002 Precision Time Protocol domain.
This enables all devices in the network implementing this standard, to synchronize their clocks with an accuracy in the sub-microsecond range.

Frame preemption can be used to interrupt the transmission of an Ethernet frame to free the link for more important traffic.
This is done to shorten the delay of a high priority package arriving and being transmitted, when a low priority package is already in transmission.

From IEEE 802.1Qav, only the so called Credit Based Shaper is relevant.
It is used to smooth the send rate of real-time packages in AVB, and can be used to smooth traffic from any traffic class in IEEE 802.1Qbv.

\begin{figure}[htbp]
  \centering
  \def\svgscale{0.7}
  \includesvg{switch}
  \caption{Schematic of a Switch implementing IEEE 802.1Qbv}
  \label{fig:tsn-switch}
\end{figure}

Finally and most importantly, IEEE 802.1Qbv standard defines the extended behavior of a switch to enable soft and hard real-time traffic along with best effort packages.
The schematic of a switch implementing IEEE 802.1Qbv can be seen in figure~\ref{fig:tsn-switch}.
All packages in a Time-Sensitive network are mapped to a certainly prioritized queue in each TSN switch in the network.
This can be done by the 3 bit \emph{Priority Code Point} (PCP), which is part of the VLAN tag of an Ethernet package, but IEEE 802.1Qbv doesn't limit the mapper to only use the PCP.
After every queue, there is a so called traffic shaper, which can smooth the traffic as described by IEEE 802.1Qav, but to not use any traffic shaping is also permitted.
The standard traffic shaper is the \emph{Credit-Based Shaper} (CBS), defined in IEEE 802.1Qav.
The shaper can be configured for each queue individually.
After the shaper, there is a gate, which controls if a queue can forward packages.
These gates are labeled with "TAS gates" in the figure to distinguish them from the traffic shaper.
The are opened and closed periodically according to the switch's configuration.
It is possible, that all gates are open at a given moment, but typically the gates are used to reserve bandwidth for more important packages by closing the low priority gates.
All gates are connected with the transmission selection component, which selects the package to transmit next.
It will select the package with the highest priority from the queues, which also indicates, that it is ready to send and which has an open gate.
It also takes the length of the package in account, so only packages are selected, that will finish their transmission before their queue's TAS gate closes~\cite{ull_2019}.
The whole TAS gate, clock, shaper and transmission selection module is referred by \emph{Time-Aware Shaper} (TAS) and reduces the jitter for high priority traffic significantly according to the simulations in this~\cite{nesting_2019} paper.

\section{\omnet}\label{sec:omnet}
\omnet{} is a discrete event simulation environment.
It is public-source and can be used for free for non-profit use.
\omnet{} is designed as a simulation library for networks, meant in a broader sense.
This leads to the framework approach, which means, that \omnet{} doesn't provide simulation components, but rather the basic machinery to build and run them.
Domain-specific functionality, such as support for Internet protocols, is provided by model frameworks, developed as independent projects~\cite{omnet_web}.

\omnet{} models consist of modules, that communicate with message passing.
The modules are written in C++, using the simulation class library.
They can be grouped into compound modules, where the number of hierarchy levels is not limited.
The model of a whole simulation is also a self contained compound module.
The messages, that are used by the modules for communication, contain a timestamp and arbitrary data.
Modules can have parameters, that are mainly used to pass configuration data.

\omnet{} uses its \emph{topology description language} NED to define the structure of a model.
This separates the model topology from the model behavior, which is written in C++.
To avoid name space clashes, the NED language uses a Java-like package structure.

In \omnet, there are two models for functionality:
\emph{Coroutine based} means, that each handler runs a function, that typically never returns, but contains send and receive calls, on its own thread.
This requires a CPU stack per component, which doesn't scale well with many modules.
The other approach, the \emph{event-processing function}, is directly called by the simulation kernel and returns after completing its task.
Also there are module initialization and finalization functions.

\omnet{} is available for all common platforms including Linux, Mac OS/X and Windows.
On Unix, it uses the gcc toolchain, whereas on Windows it uses the Microsoft Visual C++ compiler.
It ships with an Eclipse-based IDE and a graphical runtime environment.~\cite{omnet_2008}

\section{INET}
An open-source OMNeT++ model suite for wired, wireless and mobile networks.
It contains models for the Internet stack (TCP, UDP, IPv4, IPv6, OSPF, BGP, etc.) and wired and wireless link layer protocols (e.g. Ethernet).
It implements all OSI layers (physical, link-layer, network, transport, application).~\cite{inet}

\section{\nesting}
\nesting{} is built on top of the INET and OMNeT++ stack to simulate converged Time-Sensitive networks.
It implements the time-triggered scheduling mechanism of IEEE 802.1Qbv, frame preemption (IEEE 802.1Qbu and IEEE 802.3br), and credit-based shaping (IEEE 802.1Qav).~\cite{nesting_2019}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "thesis"
%%% End: 

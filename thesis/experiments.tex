%% analyse.tex
%% $Id: analyse.tex 61 2012-05-03 13:58:03Z bless $

\chapter{Experiments}
\label{ch:exp}

\section{Scheduler}

The scheduler used for generating the TAS schedules and routing rules, is supplied with the parameter shown in table~\ref{table:tas-params}.
Those two parameters combined define the cycle length as $ slot_{length} * num_{slots} $.

The scheduler is based on Dijkstra's algorithm for finding the shortest paths.
It sets the weight of a link to one plus one for every flow, which is routed through the link.

The paths are calculated iteratively.
The scheduler then assigns every node in the path excluding the receiver the earliest possible time slot, which is not occupied yet for the node and is later then the previous one in the path.
Those time slots only influence the queue with index 7, which normally is used for real-time traffic.
Therefore the gate status of a time-slot is either \texttt{0b11111111} or \texttt{0b11111110}.
If there would be any traffic with a lower priority, real-time traffic would be prioritized, if the gate status is \texttt{0b*******1}.
To further reduce the jitter of the real-time flows in the network, the other queues could be closed, while queue 7 is open.
This would also decrease the throughput of best-effort traffic in the network.


\begin{table}
        \centering
        \rowcolors{1}{white}{lightgray}
        \begin{tabular}{| c | c | c |}
                \hline
                \textbf{Parameter Name} & \textbf{Default value} & \textbf{Description} \\
                \hline
                Slot Length & 1 ms & Length of a time slot \\
                \hline
                Number of Slots & 1000 & Number of slots per cycle  \\
                \hline
        \end{tabular}
        \caption{Parameters of the Scheduler}
        \label{table:tas-params}
\end{table}

% TODO describe how scheduler works, ...



\section{Scenarios}

In this thesis robustness refers to the property of a Time-Sensitive network to operate correctly in the presence of clock jitter in the switches and talkers.
Operating correctly means, that the end-to-end delay of the real-time flows is below a specified threshold and the end-to-end delay jitter is also low.
There are four different scenarios, that get simulated for this thesis.

As shown in table~\ref{table:sim-params}, the link bandwidth, the link delay, the switch processing delay and the simulation time are set as parameters of the simulation generation toolchain.
They are the same for all experiments in this thesis, but could be changed if needed.
There are many more parameters, which are shown in table~\ref{table:gen-params}, but the default values are fine for the experiments, so they are skipped here.

\begin{table}
        \centering
        \rowcolors{1}{white}{lightgray}
        \begin{tabular}{| c | c | c |}
                \hline
                \textbf{Parameter Name} & \textbf{Parameter Value} \\
                \hline
                Link Delay & \SI{0.1}{\micro\s} \\
                \hline
                Link Bandwidth & 1 Gbit/s  \\
                \hline
		Switch Processing Delay & \SI{5}{\micro\s} \\
		\hline
		Simulation Time & 24 h \\
		\hline
        \end{tabular}
        \caption{Parameters of the Simulations}
        \label{table:sim-params}
\end{table}

All flows, that are simulated, have the same properties.
They have a repeat time of one second, a package length of 1024 bytes and send one package per burst.
This means, that each flow will send 86400 packages in the 24 hours simulated.
The time needed to send one package can be calculated as shown in equation~\ref{eq:send-time}.
Then the theoretical minimal time slot length can be calculated with equation~\ref{eq:min-slot-len}.
If multiple packages would be transmitted per burst, the link delay and the processing delay would be added only once where as the sending time is added times the packages per burst.

\begin{align} \label{eq:send-time}
	t_{transmission} = \dfrac{length_{package}}{bandwidth_{link}} = \dfrac{\SI{1024}{bytes}}{\SI{1}{\giga Bit\per\s}} = \SI{0.000008192}{\s} \approx \SI{8.2}{\micro\s}
\end{align}

\begin{align} \label{eq:min-slot-len}
\begin{split}
	t_{slot_{min}} = delay_{link} + t_{transmission} + delay_{processing}
	\\
	= \SI{0.1}{\micro\s} + \SI{8.192}{\micro\s} + \SI{5}{\micro\s} 
	\\
	= \SI{13.292}{\micro\s}
\end{split}
\end{align}

\begin{table}
        \centering
        \rowcolors{1}{white}{lightgray}
        \begin{tabular}{| c | c | c |}
                \hline
                \textbf{Slot Length} & \textbf{Number of Slots per Cycle} & \textbf{Resulting Cycle Length} \\
                \hline
                \SI{5}{\micro\s} & 200,000 & \SI{1}{\s} \\
                \hline
                \SI{10}{\micro\s} & 100,000 & \SI{1}{\s} \\
                \hline
                \SI{16}{\micro\s} & 62,500 & \SI{1}{\s} \\
                \hline
                \SI{20}{\micro\s} & 50,000 & \SI{1}{\s} \\
                \hline
                \SI{25}{\micro\s} & 40,000 & \SI{1}{\s} \\
                \hline
                \SI{50}{\micro\s} & 20,000 & \SI{1}{\s} \\
                \hline
                \SI{100}{\micro\s} & 10,000 & \SI{1}{\s} \\
                \hline
        \end{tabular}
        \caption{Different Slot Lengths used in the Scenarios}
        \label{table:slot-lengths}
\end{table}

In all scenarios, the simulations' parameters are the length of the Time-Aware Shaper time slots, as well as the jitter of the switches and talkers.
For the sake of comparability, the slot length and number of slots per cycle are chosen, so the resulting cycle length equals one second.
The slot lengths used in the simulations are shown in table~\ref{table:slot-lengths}.
The primary question is how short can the time slots be without packages missing their scheduled slots.

\clearpage

\subsection{Scenario 1: "One Talker"}

The first scenario features a network with the four switches S11, S12, S21 and S22, as well as the two hosts H1 and H2.
Only H1 is a talker, but the packages from its flows F1 and F2 take different routes through the network to H2 as shown in figure~\ref{fig:scen_01_topology}.

\begin{figure}[htbp]
	\centering
	\def\svgwidth{11cm}
	\includesvg{scen_01_topology}
	\caption{Scenario 1 with the Hosts H1 and H2, the Switches S11, S12, S21 and S22, as well as the Flows F1 and F2}
	\label{fig:scen_01_topology}
\end{figure}

\begin{figure}[htbp]
	\centering
  	\includegraphics[width=\linewidth]{sc1-sched.png}
  	\caption{Part of the Schedule of Scenario 1}
	\label{fig:sc1-sched}
\end{figure}

The relevant part of the schedule produced by \rtman{} is shown in figure~\ref{fig:sc1-sched}.
The notation \texttt{A -> B} refers to the port of \texttt{A} linking to \texttt{B}.
The open and closed in the figure only refers to real-time traffic.
In the case \texttt{A} refers to a host, there are no gates, that could be open or closed, but the talker is told when to start sending.
Therefore the flow, to which the entry belongs to, is also given.

For other traffic, the gates are always open.
The schedule is repeated every cycle and the cycle time for the experiments is one second.
The time slot length is set as a parameter of the scheduler, but doesn't change the abstract schedule shown in the figure.

\clearpage

\subsection{Scenario 2: "Multiple Talkers"}

The second scenario features a network with the three switches S1, S2 and S3, as well as the three hosts H1, H2 and H3. 
H1 and H2 are talkers, which means they both send real-time packages to H3. The packages send by H1 and H2 belong to the flows F1 and F2, as shown in in figure~\ref{fig:scen_02_topology}.

\begin{figure}[htbp]
	\centering
	\def\svgwidth{9cm}
	\includesvg{scen_02_topology}
	\caption{Scenario 2 with the Hosts H1, H2 and H3, the Switches S1, S2 and S3 and the Flows F1 and F2}
	\label{fig:scen_02_topology}
\end{figure}

The relevant part of the schedule of scenario 2 is shown in figure~\ref{fig:sc2-sched}.
Note that the schedule doesn't force the order of the flows F1 and F2 arriving at H3 every cycle.
This means, that either of them may arrive first, depending on which one arrives first at S2.

A modified schedule, that enforces an order on the flows arriving at S3 is also shown in this figure.
Flow 2 is delayed by one slot in H2 and S2, which leads to it always arriving later than Flow 1 at S3.

\begin{figure}[htbp]
	\centering
  	\includegraphics[width=\linewidth]{sc2-sc2-mod-sched.png}
  	\caption{Part of the Original and Modified Schedule of Scenario 2}
	\label{fig:sc2-sched}
\end{figure}

% \clearpage

\subsection{Scenario 3: "Two Choke Points"}

This scenario shows the possible problems of packages from different flows being not periodic.
To provoke high delays in a network, a network with the three hosts H1, H2 and H3, as in scenario 2, was chosen.
But there are four more switches to create an additional choke point, which makes a total of the six switches S1 to S7, which is shown in figure~\ref{fig:scen_03_topology}.


\begin{figure}[htbp]
	\centering
	\def\svgwidth{12cm}
	\includesvg{scen_03_topology}
	\caption{Scenario 3 with the Hosts H1, H2 and H3, the Switches S1 to S7 and the Flows F1 and F2. The Choke Point is the Link between S3 and S4.}
	\label{fig:scen_03_topology}
\end{figure}

\newpage

The relevant part of the schedule is shown in figure~\ref{fig:sc3-sched}.
The original schedule doesn't force the packages of flow 1 to arrive at S3 before the packages of flow 2.
This race condition could lead to increased delays due to the packages of flow 1 missing their next slot at S5, because they are send after the packages of flow 2 at S3 to S4.
A modified version of the schedule is also presented in the figure.
It delays the send slots for \texttt{H2 -> S2} and \texttt{S2 -> S3} by one slot length.
This forces the packages of flow 1 to arrive at S3 before the packages of flow 2.

\begin{figure}[htbp]
	\centering
  	\includegraphics[width=\columnwidth]{sc3-sc3-mod-sched.png}
  	\caption{Part of the Schedule of Scenario 3}
	\label{fig:sc3-sched}
\end{figure}

\clearpage

\subsection{Scenario 4: "Long Chains"}

The last scenario is created to analyze the impact of longer paths in the network on the robustness.
It features two hosts and two flows, both leading from H1 to H2.
They are connected by a duplicated path of $N$ switches.
The topology used can be seen in figure~\ref{fig:scen_04_topology}.
For $N = 4$, scenario 4 is the same as scenario 1.

\begin{figure}[htbp]
	\centering
	\def\svgwidth{\columnwidth}
	\includesvg{scen_04_topology}
	\caption{Scenario 4 with the Hosts H1 and H2, the Switches S*, as well as the Flows F1 and F2}
	\label{fig:scen_04_topology}
\end{figure}

An example schedule is shown in figure~\ref{fig:sc4-sched} for $N = 8$ switches.

\begin{figure}[htbp]
	\centering
  	\includegraphics[width=\linewidth]{sc4-sched.png}
  	\caption{Part of the Schedule of Scenario 4}
	\label{fig:sc4-sched}
\end{figure}

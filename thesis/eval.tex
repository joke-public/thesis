%% eval.tex
%% $Id: eval.tex 61 2012-05-03 13:58:03Z bless $

\chapter{Evaluation}
\label{ch:eval}

The results of the scenarios are evaluated and show how the toolchain developed for this thesis can be used and what it can yield.
They can be reproduced with the \odl{} logs and topology in the \mbox{\texttt{sim-generator/examples/}} directory in the attachments.
For example simulating scenario 1 with the clock jitter varying from 0 to \SI{20}{\micro\s}, can be done by calling \mbox{\texttt{sim-generator/start\_batch.sh}} with the parameters \mbox{\texttt{-t ../topology.json -c ../../configs -l odl\_log.json -o out/}}, inside the \mbox{\texttt{sim-generator/examples/1-one-talker/slot-length-20-us}} directory.
It then uses the \odl{} log produced by a scheduler with a slot length of \SI{20}{\micro\s} for the first scenario's network topology.

\section{Scenario 1}
\label{ch:eval:sec:scenario-1}


If a slot length of \SI{5}{\micro\s} is used, no packages get transmitted, because the transmission time is $\approx$ \SI{8.2}{\micro\s} and therefore the Time-Aware Shaper won't transmit these packages.
The Time-Aware Shaper will only start sending a package if the transmission will finish, before the gate closes again.
For a slot length of \SI{10}{\micro\s}, the packages get transmitted, but the end-to-end delay is above a few seconds even without a clock jitter, due to the switch processing delay delaying the packages by \SI{5}{\micro\s} and therefore make them miss their time slot.
However in the next cycle, the stuck package can be transmitted and a new one gets queued.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc1-ts-length.png}
  \caption{Influence of the Clock Jitter on the End-to-End Delay Maximum of Flow~1 for different Slot Lengths in Scenario 1}
  \label{fig:sc1-ts-length}
\end{figure}


As shown in figure~\ref{fig:sc1-ts-length}, the slot length of the scheduler greatly impacts the end-to-end delay of the flows.
The figure only shows flow~1 because flow~2 has nearly the same maximum end-to-end delays.
If the slot length is smaller than the minimal slot length calculated in equation~\ref{eq:min-slot-len}, then the packages can't be transmitted in the time slot they should and the delay increases drastically.
This can be seen for the jitter of \SI{8}{\micro\s} and the slot length of \SI{16}{\micro\s}, where the end-to-end delay maximum jumps from under \SI{100}{\micro\s} to approximate \SI{100}{\s}.

As the time slot length increases, the jitter can be larger before the end-to-end delay increases significantly.
This happens, because the time the gate for real-time traffic is open, can then be shorter than the time needed for transmitting the packages, which is \SI{8.2}{\micro\s} according to equation~\ref{eq:send-time}.

\begin{align}
  \label{eq:max-jitter}
  jitter_{max} = t_{slot} - n * t_{transmission}
\end{align}

This leads to equation~\ref{eq:max-jitter}, which defines the maximum jitter, that only influences the end-to-end delay linearly.
The maximum jitter $ jitter_{max}  $ depends on the slot length $ t_{slot} $ of the scheduler as well as the number of packages per burst $ n $  and the transmission time per package $ t_{transmission} $.
Also an increased slot lengths increases the end-to-end delay if the clock jitter is below the breaking threshold.
Table~\ref{table:jitter-max} shows the $ jitter_{max}  $ for one package per burst and a transmission time of \SI{8.2}{\micro\s} per package.
Those values exactly match the observations in figure~\ref{fig:sc1-ts-length}.

\begin{table}
  \centering
  \rowcolors{1}{white}{lightgray}
  \begin{tabular}{| c | c | c |}
    \hline
    \textbf{Slot Length} & \textbf{$jitter_{max}$} \\
    \hline
    \SI{16}{\micro\s} & \SI{7.8}{\micro\s} \\
    \hline
    \SI{20}{\micro\s} & \SI{11.8}{\micro\s} \\
    \hline
    \SI{25}{\micro\s} & \SI{16.8}{\micro\s} \\
    \hline
    \SI{50}{\micro\s} & \SI{41.8}{\micro\s} \\
    \hline
  \end{tabular}
  \caption{$jitter_{max}$ calculated for a Transmission Time of \SI{8.2}{\micro\s} and one Package per Burst}
  \label{table:jitter-max}
\end{table}

The maximum delay shows a slight decrease for the schedule with a slot length of \SI{16}{\micro\s} above a clock jitter of \SI{16}{\micro\s}.
This most likely happens, because more and more packages don't get received by their destination, leading to a smaller probability for large end-to-end delays.
This assumption coincidences with figure~\ref{fig:sc1-ts-pk-rec}, which shows, that the number of packages received by H3 decreases drastically above a clock jitter of \SI{7}{\micro\s} for a slot length of \SI{16}{\micro\s}.

The number of packages send in a simulation is \SI{1}{package\per\s} $*$ \SI{24}{h} = 86400, which can be seen in figure~\ref{fig:sc1-ts-pk-rec}.
However this number starts decreasing as soon as the jitter is above the $ jitter_{max}$ value.
This happens because packages getting stuck in the switches so long, that the switches queues get full and start dropping packages.
Also some packages might be in flight and not received by H3 yet, due to their high queueing time in the switches.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc1-ts-pk-rec.png}
  \caption{Influence of the Clock Jitter on the Number of Packages of Flow~1 received for different Slot Lengths in Scenario 1}
  \label{fig:sc1-ts-pk-rec}
\end{figure}

For a jitter below the $jitter_{max}$, the maximum delay rises by the clock jitter and the minimum delay decreases by the clock jitter, which can be seen in figure~\ref{fig:sc1-nbr-jitter}.
Furthermore the maximum, minimum and mean delay for flow~1 are all higher by a constant value for longer slot lengths.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc1-nbr-jitter.png}
  \caption{Influence of the Clock Jitter on the End-to-End Delay Maximum of Flow~1 for different Slot Lengths in Scenario 1}
  \label{fig:sc1-nbr-jitter}
\end{figure}

\newpage

\section{Scenario 2}
\label{ch:eval:sec:scenario-2}

For a clock jitter below $jitter_{max}$, the end-to-end delays can be seen in figure~\ref{fig:sc2-nbr-jitter}.
The mean and maximum delay does a slight jump from 0 to \SI{1}{\micro\s} clock jitter, because the original schedule shown in figure~\ref{fig:sc2-sched} doesn't enforce the order of the flows arriving at S3.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc2-nbr-jitter.png}
  \caption{Influence of the Clock Jitter on the End-to-End Delay Maximum of Flow~1 for different Slot Lengths in Scenario 2}
  \label{fig:sc2-nbr-jitter}
\end{figure}

This leads to the much higher difference between the maximum and minimum delay for the flows of scenario 2 compared to scenario 1, which is shown in figure~\ref{fig:sc1-sc2-end-to-end-jitter}.
This increase matches the transmission time of a package of \SI{8.2}{\micro\s} and would further increase, if the link bandwidth was less then the used 1 GBit per second.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc1-sc2-end-to-end-jitter.png}
  \caption{Maximum - Minimum End-to-End Delay of Scenario 1 and 2 for the Slot Length \SI{20}{\micro\s}}
  \label{fig:sc1-sc2-end-to-end-jitter}
\end{figure}

This can be fixed with the modified schedule presented in figure~\ref{fig:sc2-sched}.
The end-to-end delay jitter is decreased by about \SI{8.2}{\micro\s} and therefore on the same level as scenario~1, which can be seen in figure~\ref{fig:sc2-sc2-mod}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc2-sc2-mod-end-to-end-jitter.png}
  \caption{Maximum - Minimum End-to-End Delay of Scenario 2 with the modified Schedule for the Slot Length \SI{20}{\micro\s}}
  \label{fig:sc2-sc2-mod}
\end{figure}

Other than that, the $jitter_{max}$ for the different slot lengths are the same as stated in table~\ref{table:jitter-max}, which can be seen in figure~\ref{fig:sc2-ts-length}.
Flow~2 has nearly the same end-to-end delays as flow~1 in scenario 2, so it is skipped in the figure like it is in scenario~1.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc2-ts-length.png}
  \caption{Influence of the Clock Jitter on the End-to-End Delay Maximum of Flow~1 for different Slot Lengths in Scenario 2}
  \label{fig:sc2-ts-length}
\end{figure}

\newpage

\section{Scenario 3}
\label{ch:eval:sec:scenario-3}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc3-ts-length.png}
  \caption{Influence of the Clock Jitter on the End-to-End Delay Maximum of Flow~1 for different Slot Lengths in Scenario 3}
  \label{fig:sc3-ts-length}
\end{figure}

Figure~\ref{fig:sc3-ts-length} shows the impact of the clock jitter on the end-to-end delay for scenario~3.
The maximum end-to-end delay of flow~1 increases to \SI{1}{\s} for a slot length of \SI{16}{\micro\s} above a jitter of \SI{1}{\micro\s}.
This happens because the packages of flow~2 can arrive before the packages of flow~1 at S3, meaning that flow~1 is delayed there and misses it's time slot in S6.
For a larger slot length, this effect can be reduced or removed, which can be seen in the figure for the slot lengths \SI{20}{\micro\s} and \SI{25}{\micro\s}.
This does however only solve the problem for a certain amount of packages, that arrive at a choke point concurrently.
If there would be more flows or more packages per burst, this problem would occur again.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc3-mod-ts-length.png}
  \caption{Influence of the Clock Jitter on the End-to-End Delay Maximum of Flow~1 for different Slot Lengths in Scenario 3 with the modified Schedule}
  \label{fig:sc3-mod-ts-length}
\end{figure}

The modified schedule presented in figure~\ref{fig:sc3-sched} addresses this problem by enforcing the right order on the flows arriving at S3.
This can be seen in figure~\ref{fig:sc3-mod-ts-length} as the maximum end-to-end delay of flow~1 with a slot length of \SI{16}{\micro\s} doesn't increase above \SI{1}{\s} for a clock jitter below \SI{8}{\micro\s}.
With that, the $jitter_{max}$ is the same to the previous scenarios and matches table~\ref{table:jitter-max} for all slot lengths shown.

\newpage

\section{Scenario 4}
\label{ch:eval:sec:scenario-4}

The first observation is, that the toolchain takes significantly longer to simulate this scenario in comparison to the previous ones.
This can be explained by the fact, that every package sending process in \omnet{} is an own event and longer paths result in more events per simulation time.
A run with the topology shown in figure~\ref{fig:scen_04_topology} and 62 switches takes about \SI{900}{\s}, whereas a run of scenario 1 only takes about \SI{105}{\s}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc4-ts-length.png}
  \caption{Influence of the Clock Jitter on the End-to-End Delay Maximum of Flow~1 for different Slot Lengths in Scenario 4}
  \label{fig:sc4-ts-length}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{sc4-sc1-nbr-jitter.png}
  \caption{Influence of the Clock Jitter on the End-to-End Delay Maximum of Flow~1 for the Slot Lengths \SI{16}{\micro\s} in Scenario 1 and 4}
  \label{fig:sc4-sc1-nbr-jitter}
\end{figure}

The end-to-end delays displayed in figure~\ref{fig:sc4-ts-length} show, that the $ jitter_{max} $ for scenario 4 matches the values calculated in table~\ref{table:jitter-max}.
Also scenario 4 has higher end-to-end delays for jitters below $jitter_{max}$, due to the longer paths from H1 to H2.
This can be seen in figure~\ref{fig:sc4-sc1-nbr-jitter}, where the end-to-end delays of flow~1 are shown for scenario 1 and 4 for a slot length of \SI{16}{\micro\s}.
The mean delay for scenario 4 with 62 switches is about $\SI{16}{\micro\s} * 32 + \SI{8.2}{\micro\s} = \SI{512}{\micro\s} $ for a slot length of \SI{16}{\micro\s}, because there are 32 switches on a path from H1 to H2 and it takes \SI{8.2}{\micro\s} to send a package.

Also the end-to-end delays for jitters above $jitter_{max}$ are larger than the respective delays of scenario 1.
This happens because the packages from the flows in scenario 4 are more often forwarded before they reach their destination H2.
This increases the probability of a package getting into a switch, which has its slot length decreased below the transmission time due to the clock jitter, which means the package has to wait a cycle.

The distance between maximum and minimum delay for a jitter below $jitter_{max}$ for scenario 4 with 62 switches is the same as for scenario 1.
This means, that the path length doesn't has an impact on the end-to-end delay jitter, if the clock jitter is below $jitter_{max}$.

%% ==============================
\section{Conclusion}
\label{ch:eval:sec:conclusion}

%% ==============================
%% \label{ch:evaluation:sec:conclusion}

The toolchain implemented for this thesis meets the requirements from chapter~\ref{ch:Analysis}, as it is able to generate a simulation from an \odl{} log and a Mininet topology.
\nesting{} was forked and changed to be able to use the VID for routing, logging the VID of send packages as their stream identifier and be able to simulate a non additive clock jitter for the talkers and the switches.

The usage of the toolchain is demonstrated in chapter~\ref{ch:exp} and this chapter.
It is used to simulate different networks with different switch and talker jitters.
The simulations are generated from the \odl{} log in combination with the Mininet topology.
For the modified schedules, the intermediate files generated by the \parser{} were edited and then used to generate new simulations.

The schedules generated by \rtman{} were simulated and some flaws were discovered.
Also some solutions to the flaws of the simulated schedules are proposed, although no better scheduling algorithm was developed.
This was done with the statistics generated by the \analysis{} tool, which present the result of a simulation in a small JSON file, containing the maximum, minimum, mean and median end-to-end delay of all flows.
It also contains the variance, standard deviation and interval size of the end-to-end delays of the flows.

This all shows that the toolchain can be used to simulate and analyse TSN network configurations produced by a scheduler.
Also it is shown, that flaws of a schedule can be found and possible solutions can be tested with the toolchain.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End: 

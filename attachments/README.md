# Attachments to the Bachelor Thesis Simulation of Time-Sensitive Networks configured by Software-Defined Networking

First extract the archive on the disk using ``tar -xzf -C [location]``.
Then navigate to the cd directory. It contains the described files structure.

If somebody would try to run the toolchain, i highly advise to just clone it freshly with 
``git clone https://gitlab.com/joke-public/sim-generator.git``.

There are four directories in the attachments:

* nesting
* sim-generator
* sim-results
* thesis

## nesting

Contains the source code of the Nesting fork.
The source code is also available at [gitlab.com/joke-public/nesting](https://gitlab.com/joke-public/nesting.git)

## sim-generator

Contains the source code of the simulation generation toolchain, which is also available at 
[gitlab.com/joke-public/sim-generator](https://gitlab.com/joke-public/sim-generator.git).
It also contains all software needed to run the toolchain except the Omnet++ framework.
This needs to be installed onto the disk of a user. 
A manual for the ``sim-generation`` can be found in ``sim-generator/README.md``.

Note, that for running the toolchain, the ``sim-generator`` directory needs to be **writable!**

## sim-results

Contains the results of the simulations used to write the evaluation chapter.
Also online available at [gitlab.com/joke-public/sim-results](https://gitlab.com/joke-public/sim-results.git)

## thesis

Contains the latex sources of the thesis in the ``thesis/thesis`` directory.
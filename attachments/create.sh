#!/usr/bin/env bash

SCRIPT=$(readlink -f "$0")
script_path=$(dirname "$SCRIPT")

if [[ -z "$1" ]]
then
	echo -e "\e[31mError: no target specified! Aborting\e[0m";
	exit 1;
fi

echo "creating attachments to $1"

cp ${script_path}/* $1
rm create.sh

cd $1
mkdir data
cd data

git clone https://gitlab.com/joke-public/nesting.git
git clone https://gitlab.com/joke-public/sim-results.git
git clone https://gitlab.com/joke-public/sim-generator.git
cd sim-generator
git submodule init && git submodule update

cd ..
git clone git@git.scc.kit.edu:tm-stud-thesis/2019-ba-jonas-kett/thesis.git
cd thesis/thesis
./build.sh thesis

cd ../../..
tar -czf attachments.tar.gz data
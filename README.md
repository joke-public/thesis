# Thesis

## /thesis

Contains the thesis tex files, images and svgs.

## /presentation

Contains the presentation tex files.
Copies the image and svg directory from the thesis dir.

# /attachments

Contains a script to generate the attachment data.

# /misc

Contains the request for more time.

